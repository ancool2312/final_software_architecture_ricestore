const Storage = require("../model/storage");
const bcrypt = require("bcrypt");
const axios = require('axios');
const Product = require('../model/product')

const addStorage = async (req, res) => {
    try {
        const { productId, quantity } = req.body;
        const product = await Product.findById(productId);
        console.log("Product",product)
        if(!product){
            res.status(400).json({ error: error.message });
        }
        const productName = product.productName;
        const storage = new Storage({ product: productId,productName, quantity });
        await storage.save();
        res.status(201).json(storage);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

const getAllStorage = async (req, res) => {
    try {
        const storages = await Storage.find();
        res.json(storages);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

const getStorageById = async (req, res) => {
    try {
        const storage = await Storage.findOne({ productId: req.params.productId });
        if (!storage) {
            return res.status(404).json({ message: 'Storage not found' });
        }
        res.json(storage);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};
const updateStorage = async (req, res) => {
    try {
        const { quantity } = req.body;
        const storage = await Storage.findOneAndUpdate(
            { productId: req.params.productId },
            { quantity },
            { new: true }
        );
        if (!storage) {
            return res.status(404).json({ message: 'Storage not found' });
        }
        res.json(storage);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

const deleteStorage = async (req, res) => {
    try {
        const storage = await Storage.findOneAndDelete({ productId: req.params.productId });
        if (!storage) {
            return res.status(404).json({ message: 'Storage not found' });
        }
        res.json({ message: 'Storage deleted' });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

const test = async (req, res) => {
    res.send('Hello World!');
};


module.exports = {
    addStorage,
    getAllStorage,
    getStorageById,
    updateStorage,
    deleteStorage,
    test,
};
