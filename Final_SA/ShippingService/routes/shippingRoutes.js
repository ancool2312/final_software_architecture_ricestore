const { Router } = require("express");
const shippingController = require("../controller/shippingController");

const router = Router();

router.post("/addShipping", shippingController.addShipping);
router.get("/getShippingById/:id", shippingController.getShippingById);

router.get("/",shippingController.test)


module.exports = router;
