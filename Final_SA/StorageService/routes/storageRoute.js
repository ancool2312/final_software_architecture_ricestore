const { Router } = require("express");
const storageController = require('../controller/storageController');

const router = Router();

// Định nghĩa các route cho lưu trữ
router.post('/addStorage', storageController.addStorage);
router.get('/getStorageById/:productId', storageController.getStorageById)
router.put('/updateStorage/:productId', storageController.updateStorage);
router.delete('/:productId', storageController.deleteStorage);
router.get('/getAllStorage', storageController.getAllStorage); // Route mới để lấy tất cả các bản ghi

module.exports = router;
