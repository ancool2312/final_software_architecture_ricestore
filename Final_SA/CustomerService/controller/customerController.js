const Customer = require("../model/customers");
const axios = require('axios');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require('dotenv').config({ path: '.env' });


const login = async (req, res) => {
    const { username, password } = req.body;
    try {
        const user = await Customer.findOne({
            username: username
        });
        console.log(user);
        if (!user) {
            return res.status(400).json({ error: "Username is incorrect" });
        }
        const validPassword = await bcrypt.compare(password, user.password);
        if (!validPassword) {
            return res.status(400).json({ error: "Password is incorrect" });
        }
        if (user && validPassword) {
            const accessToken = jwt.sign({
                id: user._id,
                username: user.username,
                role : user.role
            },
                process.env.JWT_ACCESS_KEY,
                { expiresIn: "1000s" },
            );
            const maxAge = 86400000; // Thời gian sống là 24 giờ (24 * 60 * 60 * 1000)
            res.cookie('accessToken', accessToken, { maxAge: maxAge });
            const { password, ...info } = user._doc;
            res.status(200).json({ ...info, accessToken });
        }
    }
    catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};


const signup = async (req, res) => {
    const { name,phone,gender,email,address ,username, password, role  } = req.body;
    try {
        // Kiểm tra xem username đã tồn tại trong cơ sở dữ liệu chưa
        const user = await Customer.findOne({ username: username });
        if (user) {
            return res.status(400).json({ error: "Username is already taken" });
        }
        // Tạo salt và hash password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);
        // Tạo tài khoản mới cho khách hàng
        const newUser = new Customer({
            name: name,
            username: username,
            password: hashedPassword,
            gender: gender,
            phone: phone,
            email: email,
            address: address,
            role: role
        });
        // Lưu tài khoản mới vào cơ sở dữ liệu
        await newUser.save();
        // Trả về tài khoản mới đã được tạo
        res.status(200).json(newUser);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getAllCustomer = async (req, res) => {
    try {
        const customers = await Customer.find();
        const arrCustomer = [];
        for (const customer of customers) {
            const newCustomer = ({
                _id: customer._id,
                name: customer.name,
                phone: customer.phone,
                gender: customer.gender,
                email: customer.email,
                address: customer.address,
                username: customer.username,
                password: customer.password,
                role: customer.role,
            });
            arrCustomer.push(newCustomer);
        }
        res.status(200).json(arrCustomer);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
}


const findCustomerByID = async (req, res) => {
    const customerId = req.params.id; // Lấy ID của khách hàng từ request parameters
    try {
        const customer = await Customer.findById(customerId);
        if (!customer) {
            return res.status(404).json({ error: "Customer not found" });
        }
        const formattedCustomer = {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone,
            gender: customer.gender,
            email: customer.email,
            address: customer.address,
            username: customer.username,
            password: customer.password,
            role: customer.role,
        };
        res.status(200).json(formattedCustomer);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};



module.exports = {
    login,
    signup,
    getAllCustomer,
    findCustomerByID
};
