// models/shipping.js
const mongoose = require('mongoose');

const shippingSchema = new mongoose.Schema({
    productName: { type: String, required: true },
    quantity: { type: Number, required: true },
    address: { type: String, required: true },
    status: { type: String, default: 'Pending' }
});

const Shipping = mongoose.model('Shipping', shippingSchema);

module.exports = Shipping;
