const Product = require("../models/product");
const Storage = require("../models/storage");
const Shipping = require("../models/shipping");

const addProduct = async (req, res) => {
    const { proName } = req.body;
    try {
        const product = new Product({ proName });
        await product.save();
        res.status(200).json(product);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getAllProduct = async (req, res) => {
    try {
        const products = await Product.find();
        res.status(200).json(products);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getProductById = async (req, res) => {
    const { id } = req.params;
    try {
        const product = await Product.findById(id);
        if (!product) {
            return res.status(404).json({ error: "Product not found" });
        }
        res.status(200).json(product);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

// Storage functions
const addProductQuantity = async (req, res) => {
    const { productName, quantity } = req.body;
    try {
        const storage = new Storage({ productName, quantity });
        await storage.save();
        res.status(200).json({ message: "Quantity added successfully" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const updateProductQuantity = async (req, res) => {
    const { productName, quantity } = req.body;
    try {
        const storage = await Storage.findOne({ productName });
        if (storage) {
            storage.quantity += quantity;
            await storage.save();
            res.status(200).json({ message: "Quantity updated successfully" });
        } else {
            res.status(404).json({ error: "Product not found in storage" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getProductQuantity = async (req, res) => {
    const { productName } = req.params;
    try {
        const storage = await Storage.findOne({ productName });
        if (storage) {
            res.status(200).json({ productName, quantity: storage.quantity });
        } else {
            res.status(404).json({ error: "Product not found in storage" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const deleteProductQuantity = async (req, res) => {
    const { productName } = req.params;
    try {
        const result = await Storage.deleteOne({ productName });
        if (result.deletedCount > 0) {
            res.status(200).json({ message: "Quantity deleted successfully" });
        } else {
            res.status(404).json({ error: "Product not found in storage" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

// Shipping functions
const addShipping = async (req, res) => {
    const { productName, quantity, address } = req.body;
    try {
        const shipping = new Shipping({ productName, quantity, address });
        await shipping.save();
        res.status(200).json({ message: "Shipping added successfully" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const updateShippingStatus = async (req, res) => {
    const { shippingId, status } = req.body;
    try {
        const shipping = await Shipping.findById(shippingId);
        if (shipping) {
            shipping.status = status;
            await shipping.save();
            res.status(200).json({ message: "Shipping status updated successfully" });
        } else {
            res.status(404).json({ error: "Shipping record not found" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const getShippingById = async (req, res) => {
    const { shippingId } = req.params;
    try {
        const shipping = await Shipping.findById(shippingId);
        if (shipping) {
            res.status(200).json(shipping);
        } else {
            res.status(404).json({ error: "Shipping record not found" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const deleteShipping = async (req, res) => {
    const { shippingId } = req.params;
    try {
        const result = await Shipping.deleteOne({ _id: shippingId });
        if (result.deletedCount > 0) {
            res.status(200).json({ message: "Shipping deleted successfully" });
        } else {
            res.status(404).json({ error: "Shipping record not found" });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

const test = async (req, res) => {
    res.send('Hello World!');
};

module.exports = {
    addProduct,
    getAllProduct,
    getProductById,
    addProductQuantity,
    updateProductQuantity,
    getProductQuantity,
    deleteProductQuantity,
    addShipping,
    updateShippingStatus,
    getShippingById,
    deleteShipping,
    test,
};
