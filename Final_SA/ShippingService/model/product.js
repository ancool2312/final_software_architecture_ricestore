// models/product.js
const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    proName: { type: String, required: true, unique: true }
});

const Product = mongoose.model('Product', productSchema);

module.exports = Product;
