// models/storage.js
const mongoose = require('mongoose');

const storageSchema = new mongoose.Schema({
    productName: { type: String, required: true, unique: true },
    quantity: { type: Number, required: true }
});

const Storage = mongoose.model('Storage', storageSchema);

module.exports = Storage;
