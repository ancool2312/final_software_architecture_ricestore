const express = require('express');
const router = express.Router();
const orderController = require('../controller/orderController');

// Order routes
router.post('/placeOrder', orderController.placeOrder);
router.get('/orders', orderController.getAllOrders);
router.get('/orders/:orderId', orderController.getOrderById);
router.put('/orders/:orderId', orderController.updateOrder);
router.delete('/orders/:orderId', orderController.deleteOrder);

// Test route
router.get('/test', orderController.test);

module.exports = router;
