const { Router } = require("express");
const customerController = require("../controller/customerController");
const middlewareController = require("../middleware/middlewareController")

const router = Router();

// http://localhost:3000/api/v1/users/login
router.post("/login", customerController.login);
// http://localhost:3000/api/v1/users/sinup
router.post("/signup", customerController.signup);
// lấy ra danh sách bạn bè của 1 user  theo id cuả user
router.get("/getAllCustomer", customerController.getAllCustomer);

// http://localhost:8081/api/v1/customer/getAllCustomer
router.get("/getAllCustomer1", middlewareController.verifyTokenAuth, customerController.getAllCustomer);



router.get("/getCustomerById/:id", customerController.findCustomerByID);

router.get("/error-endpoint", (req, res) => {
    res.status(500).send('This is a forced error');
});
module.exports = router;
