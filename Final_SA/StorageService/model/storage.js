const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const storageSchema = new Schema({
    product: [{ type: mongoose.Schema.Types.ObjectId, ref: "Product" }],
    quantity: {
        type: Number,
        required: true
    }
});

const Storage = mongoose.model("Storage", storageSchema);
module.exports = Storage;