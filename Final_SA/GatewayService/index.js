const express = require("express");
const app = express();
require("dotenv").config();
const cors = require("cors");
const cookieParser = require("cookie-parser");
const httpProxy = require("http-proxy");
const bodyParser = require("body-parser");
const middleware = require("./middleware/middlewareController");
const proxy = httpProxy.createProxyServer();
const breaker = require("./circuitBreaker/circuitBreakerController.js");
const limiter = require("./ratelimiter/ratelimiter.js");
const ip = process.env.IP;
const port = process.env.PORT;

// Middleware setup
app.use(cookieParser());
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Proxy error handling
const handleProxyError = (err, req, res, target) => {
  console.error(`Error forwarding request to ${target}: ${err.message}`);
  res.status(500).send("Internal Server Error");
};

app.use(
  "/customer",
  limiter.clientLimiter,
  middleware.verifyToken,
  (req, res) => {
    proxy.web(req, res, { target: process.env.CUSTOMER_URL }, (err) =>
      handleProxyError(err, req, res, process.env.CUSTOMER_URL)
    );
  }
);

app.use("/product", middleware.verifyToken, (req, res) => {
  breaker.fire(req, res)
    .then(() => {
      proxy.web(req, res, { target: process.env.PRODUCT_URL }, (err) =>
        handleProxyError(err, req, res, process.env.PRODUCT_URL)
      );
    })
    .catch((err) => {
      console.error("Error:", err);
      if (!res.headersSent) {
        res.status(500).json({ message: "Service đang tạm đóng trong 10s!" });
      }
    });
});


// app.use("/order", middleware.verifyToken, (req, res) => {
//   proxy.web(req, res, { target: process.env.ORDER_URL }, (err) =>
//     handleProxyError(err, req, res, process.env.ORDER_URL)
//   );
// });
// // app.use("/payment", middleware.verifyToken, (req, res) => {
// //   proxy.web(req, res, { target: process.env.ROUTES }, (err) =>
// //     handleProxyError(err, req, res, process.env.ROUTES)
// //   );
// // });
// app.use("/shipping", middleware.verifyToken, (req, res) => {
//   proxy.web(req, res, { target: process.env.SHIPPING_URL }, (err) =>
//     handleProxyError(err, req, res, process.env.SHIPPING_URL)
//   );
// });
// app.use("/storage", middleware.verifyToken, (req, res) => {
//     proxy.web(req, res, { target: process.env.STORAGE_URL }, (err) =>
//       handleProxyError(err, req, res, process.env.STORAGE_URL)
//     );
//   });

app.listen(port, () => {
  console.log(`Server is running on: ${ip}:${port}`);
});