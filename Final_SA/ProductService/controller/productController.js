const Product = require("../model/product");
const redisClient = require("../redis/redisController");
const bcrypt = require("bcrypt");
const client = require("../redis/redisController");

const addProduct = async (req, res) => {
    const { productName, type, price } = req.body;
    try {
      const product = new Product({
        productName : productName,
        type : type,
        price : price,
      });
      await product.save();
  
      const products = await Product.find();
      // Lưu danh sách product vào Redis với thời gian hết hạn (TTL) là 1 giờ
      await redisClient.set("products", JSON.stringify(products), "EX", 3600);
      res.status(200).json(product);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Server error" });
    }
  };



 // để test vào terminal gõ redis-cli sau đó gõ GET products
 // để xem dữ liệu đã được lưu vào redis chưa
 // để xóa gõ DEL products
    // để xem thời gian hết hạn của key gõ TTL products
const getAllProduct = async (req, res) => {
    try {

        const cachedProducts = await redisClient.get('products');
        if (cachedProducts) {
            console.log('Dữ liệu có sẵn trong Redis');
            return res.status(200).json(JSON.parse(cachedProducts));
        }
        const products = await Product.find();
        // Lưu danh sách xe vào Redis với thời gian hết hạn (TTL) là 1 giờ
        console.log('Không có dữ liệu trong Redis, lưu dữ liệu vào Redis');
        await redisClient.setEx('products', 3600, JSON.stringify(products)); // TTL là 3600 giây (1 giờ)
        res.status(200).json(products);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};

// để test vào terminal gõ redis-cli sau đó gõ GET product:60aae4843ae33121e0de8503

const getProductById = async (req, res) => {
    const { id } = req.params;
    try {
        const cachedProducts = await redisClient.get(`product:${id}`);
        if (cachedProducts) {
            console.log('Dữ liệu có sẵn trong Redis');
            return res.status(200).json(JSON.parse(cachedProducts));
        }
        const product = await Product.findById(id);
        if (!product) {
            return res.status(404).json({ error: "Product not found" });
        }
        console.log('Không có dữ liệu trong Redis, lưu dữ liệu vào Redis');
        await redisClient.setEx(`product:${id}`, 3600, JSON.stringify(product)); // TTL là 3600 giây (1 giờ)
        res.status(200).json(product);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Server error" });
    }
};
const checkRedis = async (req, res) => {
    try {
        const reply = await client.ping();
        if (reply === 'PONG') {
            res.send('Redis connection is working');
        } else {
            res.status(500).send('Error checking Redis connection');
        }
    } catch (err) {
        res.status(500).send('Error checking Redis connection');
    }
};

const updateProduct = async (req, res) => {
    try {
        const { name, description, price } = req.body;
        const product = await Product.findOneAndUpdate(
            { id: req.params.id },
            { name, description, price },
            { new: true, runValidators: true }
        );
        if (!product) {
            return res.status(404).json({ message: 'Product not found' });
        }
        res.json(product);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};
const test = async (req, res) => {
    res.send('Hello World!');
};

const deleteProduct = async (req, res) => {
    try {
        const product = await Product.findOneAndDelete({ id: req.params.id });
        if (!product) {
            return res.status(404).json({ message: 'Product not found' });
        }
        res.json({ message: 'Product deleted' });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}



module.exports = {
    addProduct,
    getAllProduct,
    getProductById,
    checkRedis,
    test,
    updateProduct,
    deleteProduct,
};
