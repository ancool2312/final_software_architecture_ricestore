const rateLimit = require("express-rate-limit");

const clientLimiter = rateLimit({
  windowMs: 10 * 1000, 
  max: 3, //3 lần
  message: "Bạn đã thử quá nhiều lần, thử lại sau 10 giây",
});

const serverLimiter = rateLimit({
  windowMs: 10 * 1000, 
  max: 5,
  message: "Bạn đã thử quá nhiều lần, thử lại sau 10 giây",
  handler: (req, res) => {
    res.status(429).json({
      message: "Bạn đã thử quá nhiều lần, thử lại sau 10 giây",
    });
  },
});

module.exports = {
  clientLimiter,
  serverLimiter,
};