const { Router } = require("express");
const productController = require("../controller/productController");
const middlewareController = require("../middleware/middlewareController")

const router = Router();

router.post("/addProduct", productController.addProduct);
router.get("/getAllProduct", productController.getAllProduct);
router.get("/getProductById/:id", productController.getProductById);
router.put("/updateProduct/:id", productController.updateProduct);
router.delete("/delete",productController.deleteProduct)

router.get("/getAllProduct1", middlewareController.verifyTokenAuth, productController.getAllProduct);

router.get("/check-redis", productController.checkRedis);

router.get("/",productController.test)


module.exports = router;
