const Order = require("../model/order");
const bcrypt = require("bcrypt");
const axios = require('axios');
const Storage = require("../model/storage")
const Customer = require("../model/customers")

const placeOrder = async (req, res) => {
    try {
        const { productId, quantity, customerId } = req.body;

        // Kiểm tra số lượng sản phẩm có sẵn trong kho
        const storage = await Storage.findOne({ product: productId });
        if (!storage || storage.quantity < quantity) {
            return res.status(400).json({ error: 'Not enough stock available' });
        }

        // Lấy name của khách hàng từ collection Customer
        const customer = await Customer.findById(customerId);
        if (!customer) {
            return res.status(400).json({ error: 'Customer not found' });
        }

        const customerName = customer.name;

        // Tạo một đơn đặt hàng mới
        const order = new Order({ productId, quantity, customerName });
        await order.save();

        // Trừ bớt số lượng sản phẩm trong kho
        storage.quantity -= quantity;
        await storage.save();

        res.status(201).json(order);
    } catch (error) {
        console.error(error);
        res.status(400).json({ error: error.message });
    }
};

const getAllOrders = async (req, res) => {
    try {
        const orders = await Order.find();
        res.json(orders);
    } catch (error) {
        console.error(error);
        res.status(400).json({ error: error.message });
    }
};

const getOrderById = async (req, res) => {
    try {
        const order = await Order.findOne({ _id: req.params.orderId });
        if (!order) {
            return res.status(404).json({ message: 'Order not found' });
        }
        res.json(order);
    } catch (error) {
        console.error(error);
        res.status(400).json({ error: error.message });
    }
};

const updateOrder = async (req, res) => {
    try {
        const { quantity } = req.body;
        const order = await Order.findOneAndUpdate(
            { _id: req.params.orderId },
            { quantity },
            { new: true }
        );
        if (!order) {
            return res.status(404).json({ message: 'Order not found' });
        }
        res.json(order);
    } catch (error) {
        console.error(error);
        res.status(400).json({ error: error.message });
    }
};

const deleteOrder = async (req, res) => {
    try {
        const order = await Order.findOneAndDelete({ _id: req.params.orderId });
        if (!order) {
            return res.status(404).json({ message: 'Order not found' });
        }
        res.json({ message: 'Order deleted' });
    } catch (error) {
        console.error(error);
        res.status(400).json({ error: error.message });
    }
};

const test = async (req, res) => {
    res.send('Hello World!');
};

module.exports = {
    placeOrder,
    getAllOrders,
    getOrderById,
    updateOrder,
    deleteOrder,
    test,
};