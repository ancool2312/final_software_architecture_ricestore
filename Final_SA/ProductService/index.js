const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
require("dotenv").config();
const cors = require("cors");
const app = express();
const http = require("http");
const cookieParser = require('cookie-parser');
const server = http.createServer(app);
const ip = process.env.IP;
const port = process.env.PORT;
const mongodb = process.env.MONGODB_URI;
// routes
const productRoute = require('./routes/productRoute');

// middleware
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(cors());
app.use(cookieParser());

//RETRY
// Middleware function to retry MongoDB connection
const dbURI = process.env.MONGODB_URI;
const connectWithRetry = (dbURI, options, maxRetries = 5) => {
    let retries = 0;

    const connect = () => {
        mongoose.connect(dbURI, options)
            .then(() => {
                console.log("Connected to MongoDB");
            })
            .catch((error) => {
                console.error(`MongoDB connection error: ${error.message}`);
                if (retries < maxRetries) {
                    console.log(`Retrying connection to MongoDB... (Attempt ${retries + 1}/${maxRetries})`);
                    retries++;
                    setTimeout(connect, 5000); // Retry after 5 seconds
                } else {
                    console.error("Max retries exceeded. Unable to connect to MongoDB.");
                }
            });
    };

    connect();
};

// Connect to MongoDB with retry
const dbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};
connectWithRetry(dbURI, dbOptions);

// routes
app.use("/api/v1/product", productRoute);
app.get("/", (req, res) => {
    res.send("Hello World!");
});
app.use(function (req, res) {
    res.status(404).send("Not found");
});
// Start the server and listen for connections
server.listen(port, ip, () => {
    console.log("Server is running on IP: " + ip);
    console.log("Server is running on PORT: " + port);
    console.log("Server is running on DB: " + mongodb);
});
